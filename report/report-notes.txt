1. Introduction:
    - Bert 



- Bert history / Language model history
- general overview of transformer architecture // done
- Bert architecture
    - what is input / output? // done
    - difference to basic transformer 
- Test Setup:
    - dataset + split//georg
    - bert + classifier //georg
    - metrics used
      - recall, precision, ..... //draft (mohamed)
    - libraries // libraries used and build_model_function//next (mohamed
    - parameter combinations (baseline, etc.) // epochs (mohamed) , tokens (georg) , batch_size
- Results & Interpretation
- Conclusion


Ideen:
- sms model mit email dataset testen


todo Georg:
link zu vocab
batch size conclusion (spedd metrics done)
token size conclusion: reicht email header für spam klassifikation?
metriken: train/test zeit + hardware config (done)
abstract

todo Mohamed:
finish metrics confusion matrix (done)
describe token size (done)
plot token size time + describe output (done)
run for different optimizers (done)
run for trainable off (done) 
run for different epochs (done)
conclusion
BERT model size (BERT-Tiny, BERT-Small, BERT-base) (running)

------------------------ will see if we have time for this ------------------------
train-test split proportion
number and size of dense layers in the classifyer maybe ??
train-test split proportion