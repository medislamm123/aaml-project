Language models like BERT take pieces of text in natural language as input. We would like to do computation on that language input, for example computing a gradient, so words and sentences have to be preprocessed to have a numerical representation.


Let us assume we are given a short sentence that we want to feed into BERT:
\begin{displayquote}
    "Sorry, I'll call later in meeting."
\end{displayquote}

At first, this string is received by the input layer as is, it is then stored with its original text encoding in a tensor in order to be passed on to the next layers.


In tokenization, numbers are assigned to each word or parts thereof. There are various algorithms to choose from, but in case of BERT, a top-down WordPiece algorithm is commonly used. It has a vocabulary of the most frequent words, subwords and individual letters in the English Wikipedia and BookCorpus, and after splitting at whitespace, assigns an integer to each string. Frequent words will already be in the vocabulary as a whole and are assigned a single number, whereas words that rarely occur or contain typing errors are broken down into pieces, until a piece occurs frequently enough to be in the vocabulary \cite{tensorflow-WordPiece}.
At the beginning of each input sentence, a start token ([CLS]) is inserted, and at the end a separator token ([SEP]). For our example, the mapping looks like this:

\vspace{1em}

\begin{tabular}{c c c c c c c c c c c c }
    [CLS]   & Sorry     & ,     & I     & '     & ll    & call  & later & in    & meeting   & .     & [SEP]  \\
    $\downarrow$     & $\downarrow$      & $\downarrow$  & $\downarrow$  & $\downarrow$  & $\downarrow$  & $\downarrow$  & $\downarrow$  & $\downarrow$  & $\downarrow$      & $\downarrow$  & $\downarrow$ \\
    101     & 3374      & 1010  & 1045  & 1521  & 2222  & 2655  & 2101  & 1999  & 3116      & 1012  & 102   
\end{tabular}

\vspace{1em}

Words that occur rarely are split into subwords, for instance ``fluidous'':

\vspace{1em}

\begin{tabular}{c c c c}
    [CLS]               & fluid             & \#\#ous         & [SEP]         \\
    $\downarrow$        & $\downarrow$      & $\downarrow$  & $\downarrow$  \\
    101                 & 8331              & 3560          & 102           
\end{tabular}

\vspace{1em}

Transformers only take fixed length inputs, BERT, for instance, was pre-trained on a token length of 512 \cite{bert-paper} and it is not possible to feed in more tokens than that.
When fine-tuning, the input can also be a smaller number of tokens to reduce the computational cost, as the attention mechanism scales quadratically with input token count. In case of BERT preprocessing, the token count is specified in the model definition, and during training, every token vector of each input string has the same dimension (= token count). By default, the BERT preprocessor produces token vectors of dimension 128.
In case a string produces less than 128 tokens, a padding token (number 0) is inserted at the remaining dimensions. If the sentence produces more tokens, it is truncated.

The preprocessed output vector of our string is therefore:
\begin{equation*}
    [101, 3374, 1010, 1045, 1521, 2222, 2655, 2101, 1999, 3116, 1012, 102, 0, ..., 0]
\end{equation*}

where $101$ is the [CLS] token, $102$ is the [SEP] token, the numbers $3374$ to $1012$ correspond to the words and punctuation in the sentence and the zeros at the end are the padding to make the vector 128 dimensional.


% cite https://tfhub.dev/tensorflow/bert_en_uncased_preprocess/3
% 