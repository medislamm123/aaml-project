For both training and testing, we use the following metrics to evaluate our
model:

\underline{\textbf{Evaluation Metrics:}}
\begin{itemize}
      \item \textbf{Binary Accuracy}: The fraction of labels that are correctly
            predicted by the model (as either ham or spam) out of the total of
            predictions.
      \item \textbf{Precision}: The fraction of labels that are correctly
            predicted as spam (True Positives) out of the total of labels
            that are predicted as spam (True Positives + False Positives).
            Precision is of great significance for spam classification, as a
            low precision means, real messages are often hidden from the user
            which is
            highly undesirable.
      \item \textbf{Recall}: The fraction of labels that are correctly
            predicted as spam (True Positives) out of the total of labels that
            are actually spam (True Positives + False Negatives). Recall has
            lower priority
            than precision, as a shown spam message is annoying to the user, but
            no grave
            mistake.
            \begin{figure}[H]
                  \centering
                  \begin{tikzpicture}

                        \node[below of=n0,node distance=2.25cm] (n1) {
                              \begin{tikzpicture}[minimum
                                          width=2cm,minimum height=2cm,node
                                          distance=2cm]
                                    % Define colors for different cells
                                    \definecolor{tncolor}{RGB}{153,204,255}
                                    \definecolor{fpcolor}{RGB}{255,153,153}
                                    \definecolor{tpcolor}{RGB}{204,255,153}
                                    \definecolor{fncolor}{RGB}{255,204,153}

                                    \node (spam) {Pred. Spam};
                                    \node[right of=spam] (ham) {Pred. Ham};

                                    \node[draw,rectangle,fill=tpcolor,below
                                          of=spam,node distance=1.25cm] (n1)
                                    {TP};

                                    \node[left of=n1,node distance=1.25cm]
                                    (spam2) {\rotatebox{90} {Actual. Spam}};
                                    \node[below of=spam2] (ham2)
                                    {\rotatebox{90} {Actual. Ham}};

                                    \node[draw,rectangle,fill=fncolor,right
                                          of=n1] (n2)
                                    {FN};
                                    \node[draw,rectangle,fill=fpcolor, below
                                          of=n1] (n3)
                                    {FP};
                                    \node[draw,rectangle,fill=tncolor,right
                                          of=n3] (n4)
                                    {TN};

                              \end{tikzpicture}};
                  \end{tikzpicture}

                  \caption{Confusion Matrix}
            \end{figure}
      \item \textbf{Loss Function}: Our model uses the binary cross entropy
            loss
            function, also known as the log loss function, which is one of the
            most
            frequently used loss functions in machine
            learning~\cite{logloss-is-common,logloss-formula}.

            Let $D$ be the set of training data, $y_i$ be the true label of the
            $i$-th
            data point, and $p_i$ the probability of the $i$-th data
            point being
            spam as predicted by the model. Then the binary cross entropy loss
            function is defined as follows:
            \begin{equation}
                  LogLoss_i = -[y_i \ln(p_i) +(1-y_i) \ln(1-p_i)]
            \end{equation}
\end{itemize}
\underline{\textbf{Speed Metrics:}}
\begin{itemize}
      \item \textbf{Training Time:} With large enough models, training can
            become prohibitively expensive and very long training time can make
            model
            configurations infeasible that would otherwise achieve good
            performance. A long
            training time also makes quick iterations on model architecture
            more difficult.
            Hence, we are interested in configurations that keep the evaluation
            metrics mostly the same, but have greatly reduced training time.
      \item \textbf{Testing/Inference Time:} Once deployed, a model still has
            to satisfy time constraints. While in email spam classification,
            whether the
            user receives an email after five or ten seconds is usually not a
            decisive
            factor, inference time is of great significance to an email service
            provider
            that handles thousands of incoming emails simultaneously and cannot
            afford a
            bottleneck at the spam filter.
\end{itemize}

All of our models were trained on CPU in a Linux container with the following
specifications:

\begin{itemize}
      \item 10 logical cores, Intel Xeon Silver 4316 @ 2.30GHz
      \item 45GiB memory
\end{itemize}

Our speed metrics should therefore give an absolute estimate for the
performance only on similar hardware. However, the speedup between model
configurations should be similar, even on other hardware.