In the first attempt to fine-tune BERT, we used an SMS spam collection \cite{sms-spam} provided by the UC Irvine Machine Learning Repository. It contains around 5500 data points in total, with approximately 13\% being spam messages.

With the reference configuration, we already achieve very good results on the testing dataset. After just a single training epoch, we get the following values on our testing dataset:

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
    Cross-Entropy Loss  & Accuracy & Precision & Recall \\
    \hline
    0.1082 & 98.65 \% & 98.55 \% & 91.27 \% \\
    \hline
    \end{tabular}
    \vspace*{1em}
    \caption{Metrics on SMS test data using the reference configuration. Precision and recall refer to detection of the spam class.}
\end{table}


For spam classification, precision has more significance than recall, as it is a more serious problem if an email is hidden from the user that is not actually spam. With a precision of more than 98\%, we consider SMS classification on this dataset to be a solved problem. This made us reconsider using the SMS dataset, as we were unsure whether it was possible to reliably detect any further improvements by adjusting the hyperparameters.

There are further issues with the SMS dataset which lead us to the conclusion that it is not the most suitable for testing different BERT configurations.

Firstly, there is a large disparity between number of spam and number of ham messages (13\% spam and 87\% ham), which means an accuracy of 87\% can already be achieved by always predicting ham.

The second issue is the difference in token length between ham and spam.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{resources/sms-token count-logscale=True-2023-04-27_21-41-37.pdf}
    \caption{Token count for ham and spam on a logarithmic scale, SMS dataset.}
    \label{fig:token-count-sms}
\end{figure}

The median token count for ham is only 17, while it is 46 for spam. This is not in itself an issue, perhaps spam messages are just usually longer. However, we believe that with such a simple discerning indicator, we do not need a language model like BERT for classification, simpler models would suffice.

Yet, we are interested in testing the performance of BERT, so we chose to train and test BERT on another, larger dataset.

The dataset we landed on is the Enron email spam dataset \cite{enron-email-spam-dataset}. It has around 33000 emails that have been assigned a ham or spam class. It has a much more even split between ham and spam, with 49 \% spam having almost an equal share. The emails are also longer than the SMSs in the previous dataset, the word count more than doubles, with around 42 words per email and only 16 words per SMS in the median.

We can also plot the token count for the email dataset:

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{resources/email-token count-logscale=True-2023-04-27_13-55-01.pdf}
    \caption{Token count for ham and spam on a logarithmic scale, email dataset.}
    \label{fig:token-count-email}
\end{figure}

The diagram shows that ham and spam have almost an equal distribution, which means a model has to extract other features than just counting the input tokens in order to distinguish between the two classes.
We can also see that the maximum token count that BERT can accept as input, 512, is not a limiting factor when using the email dataset. Most emails are shorter than that and even 128 tokens cover most of the emails.

With the same simple model configuration we used with the SMS collection and one training epoch, we can achieve the following performance:
 
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
    Cross-Entropy Loss  & Accuracy & Precision & Recall \\
    \hline
    0.1014 & 96.30 \% & 96.69 \% & 95.76 \% \\
    \hline
    \end{tabular}
    \vspace*{1em}
    \caption{Metrics on email test data using the reference configuration. Precision and recall refer to detection of the spam class.}
\end{table} 

%We can see that BERT is able to achieve comparable metrics as with the SMS dataset, which suggests that BERT is indeed more capable than a simple model that only counts tokens.