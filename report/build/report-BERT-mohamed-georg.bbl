\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\providecommand{\doi}[1]{https://doi.org/#1}

\bibitem{illustrated-transformer}
The illustrated transformer (2023),
  \url{https://jalammar.github.io/illustrated-transformer/}, accessed on April
  22, 2023

\bibitem{tfdropoutlayer}
keras layers dropout (2023),
  \url{https://keras.io/api/layers/regularization\_layers/dropout/}, accessed
  on April 25, 2023

\bibitem{logloss-formula}
sklearn.metrics.log\_loss (2023),
  \url{https://scikit-learn.org/stable/modules/generated/sklearn.metrics.log\_loss.html},
  accessed on April 28, 2023

\bibitem{tensorflow-WordPiece}
Tensorflow text: Subword tokenizers (2023),
  \url{https://www.tensorflow.org/text/guide/subwords\_tokenizer}, accessed on
  April 17, 2023

\bibitem{sms-spam}
Almeida, Tiago \&~Hidalgo, J.: {SMS Spam Collection}. UCI Machine Learning
  Repository (2012), {DOI}: \url{10.24432/C5CC84}

\bibitem{bert-paper}
Devlin, J., Chang, M., Lee, K., Toutanova, K.: {BERT:} pre-training of deep
  bidirectional transformers for language understanding. CoRR
  \textbf{abs/1810.04805} (2018), \url{http://arxiv.org/abs/1810.04805}

\bibitem{enron-email-spam-dataset}
Metsis, V., Androutsopoulos, I., Paliouras, G.: Spam filtering with naive bayes
  - which naive bayes? (01 2006)

\bibitem{statista}
Petrosyan, A.: Global spam volume as percentage of total e-mail traffic from
  2011 to 2022 (2023),
  \url{https://www.statista.com/statistics/420400/spam-email-traffic-share-annual/},
  accessed: April 30 2023

\bibitem{tensorflow-text-bert}
TensorFlow: Classify text with bert.
  \url{https://www.tensorflow.org/text/tutorials/classify_text_with_bert}
  (2021), accessed: April 11 2023

\bibitem{attention2017}
Vaswani, A., Shazeer, N., Parmar, N., Uszkoreit, J., Jones, L., Gomez, A.N.,
  Kaiser, L., Polosukhin, I.: Attention is all you need (2017)

\bibitem{logloss-is-common}
Vovk, V.: The fundamental nature of the log loss function (2015)

\end{thebibliography}
